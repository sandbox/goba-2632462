CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration

INTRODUCTION
------------
The Secure Pages module Set which pages are always going to be used in secure
mode (SSL) Warning: Do not enable this module without configuring your web
server to handle SSL with this installation of Drupal

REQUIREMENTS
------------
This module requires none.

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------
 * Configure the Secure pages Module settings in
   Administration » Configuration » System » Secure pages:
